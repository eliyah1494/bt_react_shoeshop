import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return (
        <ItemShoe
          handleClick={this.props.handleChangeDetailShoe}
          data={item}
          key={index}
          handleAdd={this.props.handleAddToCart}
        />
      );
    });
  };
  //   handleChangeDetailShoe = (shoe) => {
  //     this.setState({
  //       detail: shoe,
  //     });
  //   };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
