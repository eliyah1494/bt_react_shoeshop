import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    console.log(this.props);
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <h2 className="card-text">{price}$</h2>
          </div>
          <button
            onClick={() => {
              this.props.handleClick(this.props.data);
            }}
            className="btn btn-secondary"
          >
            Xem chi tiết
          </button>
          <button
            onClick={() => {
              this.props.handleAdd(this.props.data);
            }}
            className="btn btn-danger"
          >
            Thêm vào giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
