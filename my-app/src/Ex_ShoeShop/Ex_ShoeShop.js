import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import Cart from "./Cart";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetailShoe = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };
  handleAddToCart = (shoe) => {
    let newCart = [...this.state.cart, shoe];
    this.setState({ cart: newCart });
  };
  render() {
    console.table(this.state.shoeArr);
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        {/* <div className="row">{this.renderListShoe()}</div> */}
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetailShoe={this.handleChangeDetailShoe}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
